import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { Router} from '@angular/router';
import * as $ from 'jquery';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  imgUrl: any;
  fullName: any;
  constructor(private cookieService: CookieService, private router: Router) {
    this.imgUrl = environment.imgUrl;
  }

  ngOnInit() {
    this.fullName = this.cookieService.get('user_name');
  }

  toggelFun() {
    //  document.querySelector('#leftsidebar').classList.remove('sidebar_width');
     $('.left_aside').toggle();
     setTimeout( function() {document.querySelector('#leftsidebar').classList.toggle('sidebar_width'); }, 1);

  }

  logout() {
    this.cookieService.deleteAll('/');
    this.router.navigate(['/login']);
  }


}
