import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AfterLoginPagesComponent } from './after-login-pages.component';

describe('AfterLoginPagesComponent', () => {
  let component: AfterLoginPagesComponent;
  let fixture: ComponentFixture<AfterLoginPagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AfterLoginPagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AfterLoginPagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
