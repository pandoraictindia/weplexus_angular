import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GlobalVariableService } from '../Services/global-variable.service';
import { CookieService } from 'ngx-cookie-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiServicesService } from '../Services/api-services.service';
import { interval , Subscription} from 'rxjs';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import * as $ from 'jquery';

@Component({
  selector: 'app-user-timeline',
  templateUrl: './user-timeline.component.html',
  styleUrls: ['./user-timeline.component.css']
})
export class UserTimelineComponent implements OnInit {
  duration = interval(environment.getDataApiCallDuration);
  subscription: Subscription;
  imgUrl: any;
  rightSidebarContent: any;
  authCode: any;
  response: any;
  message: any;
  rply: any;
  blogList = [];
  offset = 0;
  start = 1;
  limit = 5;
  temp: any;
  showSideBarBlock: any;
  isCollapsed: any;
  isCollapsed1: any;
  val1: any;
  userId: any;
  parrentCommentId: any;
  commenterName: any;

  constructor(private GlobalVariables: GlobalVariableService,
              private apiService: ApiServicesService,
              private spinner: NgxSpinnerService,
              private cookieService: CookieService,
              private routes: ActivatedRoute) {
    this.imgUrl = environment.imgUrl;
    this.rightSidebarContent = 0;
    this.GlobalVariables.changeMessage(0);
  }

  ngOnInit() {
    this.authCode = this.cookieService.get('auth_code');
    this.userId = this.cookieService.get('user_id');
    this.getBlogAndCourse(this.offset, this.limit);
  //   this.subscription = this.duration.subscribe(() =>
  //   this.getBlogAndCourse(this.offset, this.limit)
  //  );
  }

    // get top blogs and courses list

  getBlogAndCourse(offset, limit) {
    this.spinner.show();
    this.apiService.getBlogAndCourses(this.authCode, offset, limit, this.userId).
    subscribe(
        resultArray => {this.response = resultArray;
                        if (this.response.status === '200') {
                            this.spinner.hide();
                            this.message = this.response.message;
                            const temp1 = this.response.data;
                            console.log(temp1);
                            if (this.blogList.length > 0 && temp1.length > 0) {
                              for (let i = 0; i < temp1.length; i++) {
                                this.blogList.push(temp1[i]);
                              }
                            } else if (this.blogList.length <= 0) {
                              this.blogList = this.response.data;
                            }
                        } else {
                            this.spinner.hide();
                            this.rply = 0;
                            this.message = this.response.message;
                          }
      },
      error => { if  (error) {
        this.spinner.hide();
        this.rply = 0;
        this.message = 'Some Server Error Occured!';
      }});
  }
  onScrollDown() {
    this.offset = this.offset + 5;
    this.getBlogAndCourse(this.offset , this.limit);
  }

  onUp() {
  }

  addComment(blogId, commentId, isBlogComment) {
    let commentText;
    let parentCommentId;
    if (isBlogComment === 1) {
       commentText =  ((document.getElementById(blogId) as HTMLInputElement).value).trim();
       parentCommentId = 'null';
    } else if (isBlogComment === 0) {
      commentText =  ((document.getElementById('rply-sub-coment_' + commentId) as HTMLInputElement).value).trim();
      parentCommentId = commentId;
    }
    if (commentText.length <= 0) {
      return false;
    }
    const data = {auth_code: this.authCode, blog_id: blogId, userid: this.userId, comment: commentText,
                  parent_comment_id: parentCommentId};
    this.apiService.addComment(data).
    subscribe(
        resultArray => {this.response = resultArray;
                        if (this.response.status === '200') {
                          ((document.getElementById(blogId) as HTMLInputElement).value = '');
                          ((document.getElementById('rply-sub-coment_' + commentId) as HTMLInputElement).value = '');
                        } else {
                            this.spinner.hide();
                            this.rply = 0;
                            this.message = this.response.message;
                          }
      },
      error => { if  (error) {
        this.spinner.hide();
        this.rply = 0;
        this.message = 'Some Server Error Occured!';
      }});
  }
  // like for blog
  like(blogId) {
    const data = {auth_code: this.authCode, blog_id: blogId, userid: this.userId};
    this.apiService.addLike(data).
    subscribe(
    resultArray => {this.response = resultArray;
                    if (this.response.status === '200') {
                        this.spinner.hide();
                        this.message = this.response.message;
                        const temp1 = this.response.data;
                        console.log(temp1);
                    } else {
                        this.spinner.hide();
                        this.rply = 0;
                        this.message = this.response.message;
                      }
    },
    error => { if  (error) {
    this.spinner.hide();
    this.rply = 0;
    this.message = 'Some Server Error Occured!';
    }});
  }

  showRplyCommentBox(commentId, commenterName) {
    document.querySelector('#rply-comment_' + commentId).classList.remove('rply-comment');
    // this.parrentCommentId = commentId;
    ((document.getElementById('rply-sub-coment_' + commentId ) as HTMLInputElement).value = commenterName);
    // this.commenterName = commenterName;
  }

  // get all comments by blog id
  getAllComment(blogId, index) {
    this.spinner.show();
    this.apiService.getAllComments(blogId, this.authCode, this.userId).
    subscribe(
        resultArray => {this.response = resultArray;
                        if (this.response.status === '200') {
                            this.spinner.hide();
                            this.message = this.response.message;
                            const temp1 = this.response.data;
                            console.log(temp1);
                            this.blogList[index].comments = temp1;
                        } else {
                            this.spinner.hide();
                            this.rply = 0;
                            this.message = this.response.message;
                          }
      },
      error => { if  (error) {
        this.spinner.hide();
        this.rply = 0;
        this.message = 'Some Server Error Occured!';
      }});
  }

  // like for coment
  comentLike(blogId, comentId) {
    const data = {auth_code: this.authCode, userid: this.userId, blog_id: blogId, coment_id: comentId};
    this.apiService.addLike(data).
    subscribe(
    resultArray => {this.response = resultArray;
                    if (this.response.status === '200') {
                        this.spinner.hide();
                        this.message = this.response.message;
                        const temp1 = this.response.data;
                        console.log(temp1);
                    } else {
                        this.spinner.hide();
                        this.rply = 0;
                        this.message = this.response.message;
                      }
    },
    error => { if  (error) {
    this.spinner.hide();
    this.rply = 0;
    this.message = 'Some Server Error Occured!';
    }});
  }

  // delete comments
  deleteComment(comentId) {
    const data = {auth_code: this.authCode, comment_id: comentId, userid: this.userId};
    this.apiService.deleteComent(data).
    subscribe(
    resultArray => {this.response = resultArray;
                    if (this.response.status === '200') {
                      location.reload()
                    } else {
                        this.spinner.hide();
                        this.rply = 0;
                        this.message = this.response.message;
                      }
    },
    error => { if  (error) {
    this.spinner.hide();
    this.rply = 0;
    this.message = 'Some Server Error Occured!';
    }});
  }
  
}
