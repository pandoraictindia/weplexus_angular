import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import * as $ from 'jquery';


@Component({
  selector: 'app-right-side-bar',
  templateUrl: './right-side-bar.component.html',
  styleUrls: ['./right-side-bar.component.css']
})
export class RightSideBarComponent implements OnInit {
  imgUrl: any;
  val: any;
  constructor() {
    this.imgUrl = environment.imgUrl;
   }

  ngOnInit() {
    const self = this;
    // tslint:disable-next-line: only-arrow-functions
    $(window).bind('scroll', function() {
      const s = $('.sidebar_pos');
      const h = $('.sidebar_pos').height();
      console.log('height=' + $('.sidebar_pos').height());
      const b = h - 200 ;
      const posi = s.position();
      if ($(window).scrollTop() > (posi.top + b)) {
          self.val = 1;
      } else {
        self.val = 0;
      }
    });
  }

}
