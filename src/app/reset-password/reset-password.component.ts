import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'src/environments/environment';
import { CustomeValidation } from '../Services/custome-validation';
import { ApiServicesService } from '../Services/api-services.service';
import { EncryptionServicesService } from '../Services/encryption-services.service';
import * as $ from 'jquery';


@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  resetPasswordForm: FormGroup;
  message: any;
  response: any;
  logindata: any;
  submitted = false;
  forgotPassFormSubmitted = false;
  language: any;
  displaForm = 1;
  rply = 112;
  imgUrl: any;
  data: any;
  routParam: any;
  token: any;
  userId: any;
  constructor(private formBuilder: FormBuilder,
              private apiService: ApiServicesService,
              private spinner: NgxSpinnerService,
              private router: Router, private cookieService: CookieService,
              private encryption: EncryptionServicesService,
              private routParameter: ActivatedRoute) {
              this.imgUrl = environment.imgUrl;
              // Login form initialization
              this.resetPasswordForm = this.formBuilder.group({
                password: ['', [Validators.required, Validators.minLength(6)]],
                confirmPassword: ['', [Validators.required, Validators.minLength(6)]],
              },
              {
                validator: CustomeValidation.MatchPassword
              });
  }

  ngOnInit() {
    this.routParameter.params.subscribe(params => {
      this.token = params.token;
    });
    this.verifyToken(this.token);
  }

  verifyToken(token) {
    this.spinner.show();
    this.apiService.IsTokenValidate(token).
    subscribe(
      resultArray => { this.response = resultArray;
                       if (this.response.status === '200') {
                        console.log(this.response);
                        this.userId = this.response.data.userid;
                        this.spinner.hide();
                      } else {
                        alert(this.response.message);
                        this.spinner.hide();
                        this.router.navigate(['login']);
                      }
      },
      error => { if  (error) {
          this.rply = 0;
          this.message = this.response.message;
       }});
  }
  // convenience getter for easy access to form fields
  get f() { return this.resetPasswordForm.controls; }
  resetPassword() {
    this.submitted = true;
    this.message = '';
    if (this.resetPasswordForm.invalid) {
      return false;
    }
    this.spinner.show();
    const formData: FormData = new FormData();
    formData.append('userid', this.userId);
    formData.append('reset_code', this.token);
    formData.append('new_password', this.encryption.convertEncrypt(this.f.password.value));
    formData.append('confirm_password', this.encryption.convertEncrypt(this.f.confirmPassword.value));
    this.apiService.resetPassword(formData).
    subscribe(
        resultArray => {this.response = resultArray;
                        if (this.response.status === '200') {
                            this.spinner.hide();
                            this.rply = 1;
                            this.message = this.response.message;
                            // tslint:disable-next-line: only-arrow-functions
                            setTimeout(function() { this.router.navigate(['login']); }, 3000);
                        } else {
                            this.spinner.hide();
                            this.rply = 0;
                            this.message = this.response.message;
                          }
      },
      error => { if  (error) {
        this.spinner.hide();
        this.rply = 0;
        this.message = 'Some Server Error Occured!';
      }});
  }
  closeAlert() {
    this.rply = 1111;
  }

}
