import {AbstractControl} from '@angular/forms';
export class CustomeValidation {
        // confirm password validation
        static MatchPassword(control: AbstractControl) {
            const password = control.get('password').value;
            const confirmpassword = control.get('confirmPassword').value;
            if (password !== confirmpassword) {
                 control.get('confirmPassword').setErrors( {confirmPassword: true} );
             } else {
                 return null;
             }
        }
}
