import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GlobalVariableService {

  private rightSidebarContent = new BehaviorSubject(1);
  currentMessage = this.rightSidebarContent.asObservable();

  constructor() { }

  changeMessage(message: number) {
    this.rightSidebarContent.next(message);
  }
}
