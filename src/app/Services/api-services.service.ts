import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class ApiServicesService {

  apiUrl: any;
  api: any;
  handleError(arg0: any): any {
    throw new Error('Method not implemented.');
  }
  constructor(private http: HttpClient) {
    this.apiUrl = environment.apiUrl;
  }
  userLogin(formData: FormData) {
    this.api = this.apiUrl + '/login';
    return this.http
    .post(this.api, formData);
  }

  // user registration api
  submitRegistration(data) {
      this.api = this.apiUrl + '/user/create';
      return this.http.post(this.api, data);
  }

  // get blogs likes and comment
  getBlogLikesAndComment(data) {
    return this.http
    .post(this.apiUrl + 'adminlogin', data);
  }

  // send mail for forgot password link resetPassword
  sendMailForForgotPassWord(formData: FormData) {
    this.api = this.apiUrl + '/recover/password';
    return this.http.post(this.api, formData);
  }

  // check reset password token is validate
  IsTokenValidate(token) {
    this.api = this.apiUrl + '/verify/token/' + token;
    return this.http.get(this.api);
  }

  // reset password
  resetPassword(formData: FormData) {
    this.api = this.apiUrl + '/update/password';
    return this.http.post(this.api, formData);
  }

  // add blog submitComment
  addBlog(formData: FormData) {
    this.api = this.apiUrl + '/blog/create';
    return this.http.post(this.api, formData);
  }

    // add blog comment
    submitComment(formData: FormData) {
      this.api = this.apiUrl + '/blog/coment';
      return this.http.post(this.api, formData);
    }

    // add like comment
    submitLike(data) {
      this.api = this.apiUrl + '/blog/like';
      return this.http.post(this.api, data);
    }

    // follow user getBlogAndCourses
    followUser(data) {
      this.api = this.apiUrl + '/user/follow';
      return this.http.post(this.api, data);
    }

    // get top blogs and courses
    getBlogAndCourses(authCode, offset, limit, userId) {
      this.api = this.apiUrl + '/blogs/' + authCode + '/' + offset + '/' + limit + '/' + userId ;
      return this.http.get(this.api);
    }

    // add comment
    addComment(data) {
      this.api = this.apiUrl + '/blog/comment';
      return this.http.post(this.api, data);
    }

    // add remove like
    addLike(data) {
      this.api = this.apiUrl + '/blog/like';
      return this.http.post(this.api, data);
    }

    // get all comment
    getAllComments(blogId, authCode, userId) {
      this.api = this.apiUrl + '/getallblogscomments/' + blogId + '/' + authCode + '/' + userId;
      return this.http.get(this.api);
    }

    // delete comment
    deleteComent(data) {
      this.api = this.apiUrl + '/deleteblogcomment';
      return this.http.post(this.api, data);
    }

}
