import { TestBed } from '@angular/core/testing';

import { EncryptionServicesService } from './encryption-services.service';

describe('EncryptionServicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EncryptionServicesService = TestBed.get(EncryptionServicesService);
    expect(service).toBeTruthy();
  });
});
