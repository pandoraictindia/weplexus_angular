import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class EncryptionServicesService {
  key: any = CryptoJS.enc.Hex.parse('0123456789abcdef0123456789abcdef');
  iv: any = CryptoJS.enc.Hex.parse('abcdef9876543210abcdef9876543210');
  encrypted: any;
  plaintext: any;
  dplaintext: any;
  constructor() { }

  public convertEncrypt(text, keyValue: any = null) {
    if (keyValue != null) {
      keyValue = this.convertDecrypt(keyValue);
      keyValue = CryptoJS.enc.Hex.parse(keyValue);
      this.plaintext = CryptoJS.AES.encrypt(text, keyValue, { iv: this.iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
    } else {
      this.plaintext = CryptoJS.AES.encrypt(text, this.key, { iv: this.iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
    }
    return this.plaintext.toString();
  }
// Convert Decrypt
// tslint:disable-next-line:variable-name
public convertDecrypt(text, keyValue: any = null) {
      if (keyValue != null) {
        keyValue = this.convertDecrypt(keyValue);
        keyValue = CryptoJS.enc.Hex.parse(keyValue);
        this.dplaintext = CryptoJS.AES.decrypt(text, keyValue, { iv: this.iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7});
        this.dplaintext = CryptoJS.enc.Utf8.stringify(this.dplaintext);
      } else {
        this.dplaintext = CryptoJS.AES.decrypt(text, this.key, { iv: this.iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        this.dplaintext = CryptoJS.enc.Utf8.stringify(this.dplaintext);
      }
      return this.dplaintext.toString();
  }
  // replace Dollar By Forward
  public replaceDollarByForward(text) {
    // tslint:disable-next-line:variable-name
    const user_id = text.replace(/\$/g, '/');
    // tslint:disable-next-line:variable-name
    let repl_output = user_id.replace(/\!/g, '=');
    repl_output = repl_output.replace(/\@/g, '+');
    return repl_output;
  }
  // replace Forward By Dollar
  public replaceForwardByDollar(text) {
        // tslint:disable-next-line:variable-name
        const user_id = text.replace(/\//g, '$');
        // tslint:disable-next-line:variable-name
        let repl_output = user_id.replace(/\=/g, '!');
        repl_output = repl_output.replace(/\+/g, '@');
        return repl_output;
  }
}
