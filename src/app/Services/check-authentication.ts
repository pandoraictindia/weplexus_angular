import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { RouterModule, Routes, Router } from '@angular/router';
@Injectable()
export class CheckAuthentication {

    authCode: any;
    constructor(private cookiesService: CookieService, private routes: Router) {
        this.authCode = this.cookiesService.get('auth_code');
    }
    allowAdmin() {
        const isCookiSet = this.cookiesService.check('auth_code');
        if (isCookiSet) {
            return true;
        } else {
            this.routes.navigate(['/login']);
        }
    }

    allowUser() {
        const isCookiSet = this.cookiesService.check('auth_code');
        if (isCookiSet) {
            return true;
        } else {
            this.routes.navigate(['/login']);
        }
      }
}
