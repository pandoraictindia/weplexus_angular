import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiServicesService } from '../../Services/api-services.service';
import { EncryptionServicesService } from '../../Services/encryption-services.service';


@Component({
  selector: 'app-blog-details',
  templateUrl: './blog-details.component.html',
  styleUrls: ['./blog-details.component.css']
})
export class BlogDetailsComponent implements OnInit {

  imgUrl: any;
  commentFrom: any;
  response: any;
  tempData: any; // for temporary not use in actuallapi
  message: any;
  rply: any;
  blogId: any;
  userId: any;
  authCode: any;
  submitted = false;
  constructor(private formBuilder: FormBuilder,
              private apiService: ApiServicesService,
              private spinner: NgxSpinnerService,
              private router: Router, private cookieService: CookieService,
              private encryption: EncryptionServicesService,
              ) {
                this.imgUrl = environment.imgUrl;
                this.commentFrom = this.formBuilder.group({
                  email: ['', [Validators.required, Validators.pattern(/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i)]],
                  name: ['', [Validators.required]],
                  comment: ['', [Validators.required]],
                });
              }

  ngOnInit() {
    this.getBlogCommentAndViewCount();
    this.blogId = 1;
    this.userId = this.cookieService.get('user_id');
    this.authCode = this.cookieService.get('auth_code');
  }
  // convenience getter for easy access to form fields
  get f() { return this.commentFrom.controls; }
  // get count of Views and Like
  getBlogCommentAndViewCount() {
    this.apiService.getBlogLikesAndComment(this.tempData).
    subscribe(
        resultArray => {this.response = resultArray;
                        if (this.response.status === '200') {
                            this.spinner.hide();
                            this.message = this.response.message;
                            this.router.navigate(['login']);
                        } else {
                            this.spinner.hide();
                            this.rply = 0;
                            this.message = this.response.message;
                          }
      },
      error => { if  (error) {
        this.spinner.hide();
        this.rply = 0;
        this.message = 'Some Server Error Occured!';
      }});
  }

  submitComment() {
    this.submitted = true;
    if (this.commentFrom.invalid) {
      return false;
    }
    this.spinner.show();
    const formData: FormData = new FormData();
    formData.append('auth_code', this.authCode);
    formData.append('user_id', this.userId);
    formData.append('comment', this.f.comment.value);
    console.log(formData);
    this.apiService.submitComment(formData).
    subscribe(
        resultArray => {this.response = resultArray;
                        if (this.response.status === '200') {
                            this.spinner.hide();
                            this.message = this.response.message;
                            this.router.navigate(['login']);
                        } else {
                            this.spinner.hide();
                            this.rply = 0;
                            this.message = this.response.message;
                          }
      },
      error => { if  (error) {
        this.spinner.hide();
        this.rply = 0;
        this.message = 'Some Server Error Occured!';
      }});
  }

  // add like
  like() {
    this.submitted = true;
    if (this.commentFrom.invalid) {
      return false;
    }
    this.spinner.show();
    const data = {blogid: this.blogId, auth_code: this.authCode};
    this.apiService.submitLike(data).
    subscribe(
        resultArray => {this.response = resultArray;
                        if (this.response.status === '200') {
                            this.spinner.hide();
                            this.message = this.response.message;
                            this.router.navigate(['login']);
                        } else {
                            this.spinner.hide();
                            this.rply = 0;
                            this.message = this.response.message;
                          }
      },
      error => { if  (error) {
        this.spinner.hide();
        this.rply = 0;
        this.message = 'Some Server Error Occured!';
      }});
  }

  // add like
  followUser(blogerId) {
    this.spinner.show();
    const data = {bloger_id: blogerId, followerId: this.userId, auth_code: this.authCode};
    this.apiService.followUser(data).
    subscribe(
        resultArray => {this.response = resultArray;
                        if (this.response.status === '200') {
                            this.spinner.hide();
                            this.message = this.response.message;
                        } else {
                            this.spinner.hide();
                            this.rply = 0;
                            this.message = this.response.message;
                          }
      },
      error => { if  (error) {
        this.spinner.hide();
        this.rply = 0;
        this.message = 'Some Server Error Occured!';
      }});
  }

  onScrollDown() {
    console.log(1233);
  }
  onUp() {
    console.log(1233);
  }

}
