import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidationErrors } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'src/environments/environment';
import { ApiServicesService } from '../../Services/api-services.service';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { DatePipe } from '@angular/common';
import { GlobalVariableService } from '../../Services/global-variable.service';
// import {ClassicEditor, SimpleUploadAdapter } from '@ckeditor/ckeditor5-upload/src/adapters/simpleuploadadapter';
import EasyImage from '@ckeditor/ckeditor5-easy-image/src/easyimage';


@Component({
  selector: 'app-add-blog',
  templateUrl: './add-blog.component.html',
  styleUrls: ['./add-blog.component.css']
})
export class AddBlogComponent implements OnInit {

  imgUrl: any;
  Editor = ClassicEditor;
  addBlogForm: any;
  uploadFileName: any;
  authCode: any;
  loginUserId: any;
  message: any;
  apiResponce: any;
  rply: any;
  minDateConfig: any;
  currentDate = new Date();
  submitted = false;
  rightSidebarContent: any;
  showSidebar = 0;
  val1: any;
  editorConfig = {
    // plugins: [SimpleUploadAdapter],
    toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList',
     'blockQuote', 'ckfinder', 'imageUpload', 'mediaEmbed', 'insertTable'],
    heading: {
        options: [
            { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
            { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
            { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
            { model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
            { model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4' }
        ]
    },

    ckfinder: {
      options: {
        resourceType: 'Images'
      },
      uploadUrl: 'http://weplexus.siddhiglobal.net/weplexus_api/public/api/upload/image'
      }
  //   simpleUpload: {
  //     // The URL that the images are uploaded to.
  //     uploadUrl: 'http://example.com',

  //     // Headers sent along with the XMLHttpRequest to the upload server.
  //     headers: {
  //         'X-CSRF-TOKEN': 'CSFR-Token',
  //         Authorization: 'Bearer <JSON Web Token>'
  //     }
  // }
};



  constructor(private formBuilder: FormBuilder,
              private apiService: ApiServicesService,
              private spinner: NgxSpinnerService,
              private router: Router, private cookieService: CookieService,
              private datePipe: DatePipe, private GlobalVariables: GlobalVariableService) {
              this.imgUrl = environment.imgUrl;
              // add blog form initialization
              this.addBlogForm = this.formBuilder.group({
                title: ['', [Validators.required]],
                description: ['', [Validators.required]],
                tag: [''],
                reference: [''],
                blogtype: ['0', [Validators.required]],
                validitydate: [''],
    });
              this.GlobalVariables.changeMessage(1);
  }

  ngOnInit() {
    const date = new Date();
    const arry = this.datePipe.transform(date, 'yyyy-MM-dd').split('-');
    this.minDateConfig = {year: +arry[0], month: +arry[1], day: +arry[2]};
    console.log(this.minDateConfig);
    this.authCode = this.cookieService.get('auth_code');
    this.loginUserId = this.cookieService.get('user_id');
  }
  // convenience getter for easy access to form fields
  get f() { return this.addBlogForm.controls; }

  // upload file
  uploadFile(file: FileList) {
    this.uploadFileName = file.item(0);
  }
  onSubmit() {
    this.submitted = true;
    this.message = '';
    // stop here if form is invalid
    if (this.addBlogForm.invalid) {
      return false;
    }
    // console.log(this.addBlogForm.value);
    const tempDateObj = this.f.validitydate.value;
    const tempValidDate = tempDateObj.year + '/' + tempDateObj.month + '/' + tempDateObj.day;
    const formData: FormData = new FormData();
    formData.append('auth_code', this.authCode);
    formData.append('userid', this.loginUserId);
    formData.append('title', this.f.title.value);
    formData.append('blog_text', this.f.description.value);
    formData.append('ispremium', this.f.blogtype.value);
    formData.append('blog_references', this.f.reference.value);
    formData.append('tag', this.f.tag.value);
    formData.append('document', this.uploadFileName);
    formData.append('valid_date', tempValidDate);
    this.apiService.addBlog(formData).
    subscribe(
        resultArray => {this.apiResponce = resultArray;
                        if (this.apiResponce.status === '200') {
                            this.spinner.hide();
                            this.rply = 1;
                            this.message = this.apiResponce.message;
                            this.addBlogForm.reset();
                            window.scroll(0, 0);
                        } else {
                            this.spinner.hide();
                            this.rply = 0;
                            this.message = this.apiResponce.message;
                          }
      },
      error => { if  (error) {
        this.spinner.hide();
        this.rply = 0;
        this.message = 'Some Server Error Occured!';
      }});
  }

}
