import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HttpClient} from '@angular/common/http';

// installed
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import {DatePipe} from '@angular/common';
import { SocialLoginModule, AuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';
// import {  } from 'angularx-social-login';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { EncryptionServicesService } from './Services/encryption-services.service';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { UserTimelineComponent } from './user-timeline/user-timeline.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { AddBlogComponent } from './user-pages/add-blog/add-blog.component';
import { RightSideBarComponent } from './right-side-bar/right-side-bar.component';
import {CheckAuthentication} from './Services/check-authentication';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { AfterLoginPagesComponent } from './after-login-pages/after-login-pages.component';

const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('538134483321-9lvaurvhbf4vlh0hvav9p0tvtuicbrlm.apps.googleusercontent.com')
  },
]);
export function provideConfig() {
  return config;
}
@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    LoginComponent,
    RegistrationComponent,
    HeaderComponent,
    FooterComponent,
    UserTimelineComponent,
    SideBarComponent,
    RightSideBarComponent,
    ResetPasswordComponent,
    AfterLoginPagesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CKEditorModule,
    SocialLoginModule,
    InfiniteScrollModule
  ],
  providers: [CookieService, CheckAuthentication, EncryptionServicesService,
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
