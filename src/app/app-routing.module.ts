import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserAuthenticationGuard } from './Authentication/user-authentication.guard';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { AfterLoginPagesComponent } from './after-login-pages/after-login-pages.component';
import { UserTimelineComponent } from './user-timeline/user-timeline.component';
import { AddBlogComponent } from './user-pages/add-blog/add-blog.component';
import { BlogDetailsComponent } from './user-pages/blog-details/blog-details.component';






const routes: Routes = [
  // common pages
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent},
  { path: 'reset_password/:token', component: ResetPasswordComponent},
  { path: 'register', component: RegistrationComponent},
  { path: 'header', component: HeaderComponent},
  { path: 'footer', component: FooterComponent},

  // internal pages canActivate: [UserAuthenticationGuard]
  { path: 'userpages', component: AfterLoginPagesComponent,
   children: [
    { path: 'user-timeline/:sidebarShow', component: UserTimelineComponent},
    { path: 'add-blog/:sidebarShow', component: AddBlogComponent},
    { path: 'blog-details', component: BlogDetailsComponent},
  ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true, scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
  LoginComponent,
  HeaderComponent,
  AddBlogComponent,
  BlogDetailsComponent
];
