import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'src/environments/environment';
import { ApiServicesService } from '../Services/api-services.service';
import { EncryptionServicesService } from '../Services/encryption-services.service';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  imgUrl: any;
  registrationFrom: any;
  submitted = false;
  response: any;
  message: any;
  rply: any;
  constructor(private formBuilder: FormBuilder,
              private apiService: ApiServicesService,
              private spinner: NgxSpinnerService,
              private router: Router, private cookieService: CookieService,
              private encryption: EncryptionServicesService,
              ) {
                this.imgUrl = environment.imgUrl;
                this.registrationFrom = this.formBuilder.group({
                  email: ['', [Validators.required, Validators.pattern(/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i)]],
                  password: ['', [Validators.required, Validators.minLength(6)]],
                  firstname: ['', [Validators.required]],
                  lastname: ['', [Validators.required]],
                  number: ['', [Validators.required, Validators.pattern('^[0-9]*$'), Validators.minLength(10)]],
                });

  }

  ngOnInit() {
  }

  // convenience getter for easy access to form fields
  get f() { return this.registrationFrom.controls; }
  submitRegistrationForm() {
    this.submitted = true;
    if (this.registrationFrom.invalid) {
      return false;
    }
    this.spinner.show();
    const formData: FormData = new FormData();
    formData.append('first_name', this.f.firstname.value);
    formData.append('last_name', this.f.lastname.value);
    formData.append('email', this.f.email.value);
    formData.append('mobile', this.f.number.value);
    formData.append('pwd', this.f.password.value);
    // console.log(formData);
    this.apiService.submitRegistration(formData).
    subscribe(
        resultArray => {this.response = resultArray;
                        if (this.response.status === 200) {
                            this.spinner.hide();
                            this.message = this.response.message;
                            this.router.navigate(['login']);
                        } else {
                            this.spinner.hide();
                            this.rply = 0;
                            this.message = this.response.message;
                          }
      },
      error => { if  (error) {
        this.spinner.hide();
        this.rply = 0;
        this.message = 'Some Server Error Occured!';
      }});

  }


  // close alert
  closeAlert() {
    this.rply = 1111;
  }

}
