import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'src/environments/environment';
import { ApiServicesService } from '../Services/api-services.service';
import { EncryptionServicesService } from '../Services/encryption-services.service';
import { FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  forgotForm: FormGroup;
  message: any;
  response: any;
  logindata: any;
  submitted = false;
  forgotPassFormSubmitted = false;
  language: any;
  displaForm = 1;
  rply = 112;
  imgUrl: any;
  data: any;
  socialLoginData: any;
  isForgotPassWin = 0;
  forgotPasswordForm: any;
  thisObject = this;
  constructor(private formBuilder: FormBuilder,
              private apiService: ApiServicesService,
              private spinner: NgxSpinnerService,
              private router: Router, private cookieService: CookieService,
              private encryption: EncryptionServicesService,
              private socialAuthService: AuthService) {
              this.imgUrl = environment.imgUrl;
              // Login form initialization
              this.loginForm = this.formBuilder.group({
                email: ['', [Validators.required, Validators.pattern(/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i)]],
                password: ['', [Validators.required, Validators.minLength(6)]],
              });

              this.forgotPasswordForm = this.formBuilder.group({
                email: ['', [Validators.required, Validators.pattern(/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i)]],
              });
  }

  ngOnInit() {
  }
  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }
  get forgotPassword() { return this.forgotPasswordForm.controls; }
  login() {
    // this.signInWithGoogle();
    // const enctemp = this.encryption.convertEncrypt('test');
    // const dectemp = this.encryption.convertDecrypt(enctemp);
    this.submitted = true;
    this.message = '';
    if (this.loginForm.invalid) {
      return false;
    }
    this.spinner.show();
    const formData: FormData = new FormData();
    formData.append('email', this.f.email.value);
    formData.append('password', this.encryption.convertEncrypt(this.f.password.value));
    this.apiService.userLogin(formData).
    subscribe(
        resultArray => {this.response = resultArray;
                        if (this.response.status === '200') {
                            this.spinner.hide();
                            this.message = this.response.message;
                            this.logindata = this.response.data;
                            const fullname =  this.logindata['first_name'] + ' ' + this.logindata['last_name'];
                            this.cookieService.set('auth_code', this.logindata['auth_code'], 0, '/', '' , false, 'Lax');
                            this.cookieService.set('user_id', this.logindata['userid'], 0, '/', '' , false, 'Lax');
                            this.cookieService.set('user_name', fullname, 0, '/', '' , false, 'Lax');
                            this.cookieService.set('email', this.logindata['email'], 0, '/', '' , false, 'Lax');
                            this.cookieService.set('mobile', this.logindata['mobile'], 0, '/', '' , false, 'Lax');
                            this.router.navigate(['userpages/user-timeline/0']);
                        } else {
                            this.spinner.hide();
                            this.rply = 0;
                            this.message = this.response.message;
                          }
      },
      error => { if  (error) {
        this.spinner.hide();
        this.rply = 0;
        this.message = 'Some Server Error Occured!';
      }});
  }

  signInWithGoogle(): void {
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID).then(x => this.saveSocialLoginData(x));
  }

  saveSocialLoginData(data) {
    console.log(data);
    this.router.navigate(['user-timeline']);

  }
  // submit mail for forgot password link
  submitMail() {
    this.forgotPassFormSubmitted = true;
    // stop here if form is invalid
    if (this.forgotPasswordForm.invalid) {
      return false;
    }
    this.spinner.show();
    const formData: FormData = new FormData();
    formData.append('email', this.f.email.value);
    this.apiService.sendMailForForgotPassWord(formData).
    subscribe(
        resultArray => {this.response = resultArray;
                        if (this.response.status === '200') {
                            this.spinner.hide();
                            this.rply = 1;
                            this.message = this.response.message;
                            this.logindata = this.response.data;
                            this.isForgotPassWin = 0;
                        } else {
                            this.spinner.hide();
                            this.rply = 0;
                            this.message = this.response.message;
                          }
      },
      error => { if  (error) {
        this.spinner.hide();
        this.rply = 0;
        this.message = 'Some Server Error Occured!';
      }});
  }

}
