import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { GlobalVariableService } from '../Services/global-variable.service';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import * as $ from 'jquery';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {
  imgUrl: any;
  name: any;
  mobile: any;
  email: any;
  val: any;
  rightSidebarContent: any;
  showSideBarBlock: any;
  temp: any;
  constructor( private cookieService: CookieService,
               private GlobalVariables: GlobalVariableService,
               private routParameter: ActivatedRoute,
               private router: Router) {
    this.imgUrl = environment.imgUrl;
    this.name = this.cookieService.get('user_name');
    this.email = this.cookieService.get('email');
    this.mobile = this.cookieService.get('mobile');
    this.routParameter.params.subscribe(params => {
      this.showSideBarBlock = +params.sidebarShow;
    });
  }

  ngOnInit() {
    const self = this;
    // tslint:disable-next-line: only-arrow-functions
    $(window).bind('scroll', function() {
      const s = $('.sidebar_pos');
      const h = $('.sidebar_pos').height();
      const b = h - 200 ;
      const posi = s.position();
     //  console.log($(window).scrollTop());
      if ($(window).scrollTop() > (posi.top + b)) {
          self.val = 1;
      } else {
        self.val = 0;
      }
    });

    this.GlobalVariables.currentMessage.subscribe(rightSidebarContent => this.rightSidebarContent = rightSidebarContent);
  }

}
