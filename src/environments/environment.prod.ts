export const environment = {
  production: true,
  // imgUrl: 'http://siddhiglobal.net/acceptance/weplexus/assets/images',
  imgUrl: 'http://weplexus.siddhiglobal.net/weplexus_web/assets/images',
  // apiUrl: 'http://siddhiglobal.net/acceptance/weplexus_api/public/api',
  apiUrl: 'http://weplexus.siddhiglobal.net/weplexus_api/public/api',
  getDataApiCallDuration: 3000,
};
